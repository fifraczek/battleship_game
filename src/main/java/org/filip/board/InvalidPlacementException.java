package org.filip.board;

import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = false)
public class InvalidPlacementException extends RuntimeException {

    public InvalidPlacementException(String message) {
        super(message);
    }

    public static InvalidPlacementException notInLine() {
        return new InvalidPlacementException("Ship should be placed in line (horizontally/vertically).");
    }

    public static InvalidPlacementException fieldOccupied(String occupiedFieldNames) {
        return new InvalidPlacementException(String.format("These fields are already occupied: %s. Please pick another ones.", occupiedFieldNames));
    }

    public static InvalidPlacementException positionDuplicated(String duplicatedPositions) {
        return new InvalidPlacementException(String.format("These positions are selected more than once: %s. Please pick another ones.", duplicatedPositions));
    }

    public static InvalidPlacementException notContinuous() {
        return new InvalidPlacementException("Ship has to occupied concentrated fields, e.g. A1, A2, A3");
    }

}
