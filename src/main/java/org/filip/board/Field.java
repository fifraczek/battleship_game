package org.filip.board;

import lombok.Getter;

@Getter
public class Field {

    private boolean shipPresent;
    private boolean wasShot;

    public Field() {
        this.shipPresent = false;
        this.wasShot = false;
    }

    public void setShipPresent() {
        this.shipPresent = true;
    }

    public void setWasShot() {
        if (this.wasShot) {
            throw new AlreadyShotException();
        }
        this.wasShot = true;
    }

    public FieldStatus getFieldStatus() {
        if (this.wasShot && this.shipPresent) {
            return FieldStatus.HIT;
        } else if (this.wasShot) {
            return FieldStatus.MISSED;
        } else {
            return FieldStatus.NOT_SHOT;
        }
    }
}
