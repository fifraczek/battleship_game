package org.filip.board;

import org.filip.gui.GuiElementsBuilder;
import org.filip.ship.ShipRotation;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public class ShipPlacementValidator {

    private final Board board;
    private final Set<Position> desiredPositions;

    public ShipPlacementValidator(Board board, Set<Position> desiredPositions) {
        this.board = board;
        this.desiredPositions = desiredPositions;
    }

    public void validate() {
        validateDuplicates();
        validateContinuity();
        validateFreeSpace();
    }


    private void validateDuplicates() {
        final List<String> duplicatedPositions = this.desiredPositions.stream()
                .collect(Collectors.groupingBy(GuiElementsBuilder::getPositionLabel))
                .entrySet()
                .stream()
                .filter(o -> o.getValue().size() > 1)
                .map(Map.Entry::getKey)
                .toList();
        if (!duplicatedPositions.isEmpty()) {
            String duplicatedPositionsNames = String.join(", ", duplicatedPositions);
            throw InvalidPlacementException.positionDuplicated(duplicatedPositionsNames);
        }
    }

    private void validateContinuity() {
        final List<Integer> sortedIndices = this.desiredPositions.stream()
                .map(this.getRotation().getIndexExtractor())
                .sorted()
                .toList();
        for (int i = 0; i < sortedIndices.size() - 1; i++) {
            if (sortedIndices.get(i + 1) - sortedIndices.get(i) != 1) {
                throw InvalidPlacementException.notContinuous();
            }
        }
    }

    private ShipRotation getRotation() {
        return ShipRotation.getRotationFromPosition(desiredPositions);
    }

    private void validateFreeSpace() {
        List<Position> occupiedPositions = desiredPositions.stream()
                .filter(position -> this.board.getFieldAtPosition(position).isShipPresent())
                .toList();
        if (!occupiedPositions.isEmpty()) {
            String occupiedFieldNames = occupiedPositions.stream().map(GuiElementsBuilder::getPositionLabel)
                    .collect(Collectors.joining(", "));
            throw InvalidPlacementException.fieldOccupied(occupiedFieldNames);
        }
    }

}
