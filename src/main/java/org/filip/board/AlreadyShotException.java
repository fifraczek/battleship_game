package org.filip.board;

public class AlreadyShotException extends RuntimeException {

    public AlreadyShotException() {
        super("This position is already shot. Pick another one.");
    }
}