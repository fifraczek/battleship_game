package org.filip.board;

import lombok.Getter;
import org.filip.gui.InvalidPositionException;

@Getter
public class Position {


    private final int column;
    private final int row;

    public Position(int column, int row) {
        Board.validateIndex(column);
        Board.validateIndex(row);
        this.column = column;
        this.row = row;
    }

    public Position(String position) {
        try {
            final String columnString = position.substring(0, 1);
            final String rowString = position.substring(1);
            this.column = readColumn(columnString);
            this.row = readRow(rowString);
        } catch (IndexOutOfBoundsException | NumberFormatException e) {
            throw new InvalidPositionException();
        }
    }

    private static int readColumn(String columnString) {
        final int columnIndex = columnString.toCharArray()[0] - Board.A_ASCII_INDEX;
        Board.validateIndex(columnIndex);
        return columnIndex;
    }

    private static int readRow(String rowString) {
        final int rowIndex = Integer.parseInt(rowString) - 1;
        Board.validateIndex(rowIndex);
        return rowIndex;
    }

}
