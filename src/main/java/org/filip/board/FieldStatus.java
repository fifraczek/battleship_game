package org.filip.board;

import lombok.Getter;

@Getter
public enum FieldStatus {
    NOT_SHOT,
    HIT,
    MISSED
}
