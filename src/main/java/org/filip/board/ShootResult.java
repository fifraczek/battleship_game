package org.filip.board;

import lombok.Data;
import org.filip.ship.ShipType;

@Data
public class ShootResult {

    private final FieldStatus fieldStatus;
    private final ShipType shipType;
    private final boolean shipSunk;

}
