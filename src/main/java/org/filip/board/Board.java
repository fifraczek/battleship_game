package org.filip.board;

import lombok.Getter;
import org.filip.gui.FieldSymbol;
import org.filip.gui.InvalidPositionException;
import org.filip.gui.GuiElementsBuilder;
import org.filip.ship.Ship;
import org.filip.ship.ShipType;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;


public class Board {

    public static final int BOARD_SIZE = 10;
    public static final int A_ASCII_INDEX = 'A';
    public static final char LAST_COLUMN = (char) (Board.BOARD_SIZE + A_ASCII_INDEX);

    private final Field[][] fields;
    @Getter
    private final List<Ship> ships;

    public Board() {
        this.fields = initializeEmptyFields();
        this.ships = new ArrayList<>();
    }

    public static void validateIndex(int index) {
        if (index < 0 || index >= Board.BOARD_SIZE) {
            throw new InvalidPositionException();
        }
    }

    public void placeShip(Set<Position> positions) {
        final ShipPlacementValidator validator = new ShipPlacementValidator(this, positions);
        validator.validate();
        final Set<Field> shipFields = positions.stream()
                .map(this::getFieldAtPosition)
                .collect(Collectors.toSet());
        shipFields.forEach(Field::setShipPresent);
        Ship newShip = new Ship(shipFields);
        this.ships.add(newShip);
    }

    private Field[][] initializeEmptyFields() {
        final Field[][] fieldArray = new Field[BOARD_SIZE][BOARD_SIZE];
        for (int column = 0; column < BOARD_SIZE; column++) {
            for (int row = 0; row < BOARD_SIZE; row++) {
                fieldArray[column][row] = new Field();
            }
        }
        return fieldArray;
    }

    public Field getFieldAtPosition(Position position) {
        return this.fields[position.getColumn()][position.getRow()];
    }

    public long activeShipByType(ShipType shipType) {
        return this.ships.stream()
                .filter(ship -> shipType.equals(ship.getShipType()) && ship.isActive())
                .count();
    }

    public long shipByType(ShipType shipType) {
        return this.ships.stream()
                .filter(ship -> shipType.equals(ship.getShipType()))
                .count();
    }

    public ShootResult shootPosition(Position position) {
        final Field fieldAtPosition = getFieldAtPosition(position);
        fieldAtPosition.setWasShot();
        final Optional<Ship> shipAtPosition = getShipAtPosition(position);
        final ShipType shipType = shipAtPosition.map(Ship::getShipType).orElse(null);
        final boolean shipSunk = shipAtPosition.map(Ship::isSunk).orElse(false);
        return new ShootResult(fieldAtPosition.getFieldStatus(), shipType, shipSunk);
    }

    public boolean allShipsSunk() {
        return this.ships.stream()
                .allMatch(Ship::isSunk);
    }

    public String getSymbolOfPosition(Position position) {
        return FieldSymbol.findByStatus(getFieldAtPosition(position)
                .getFieldStatus())
                .getTextSymbol(getShipAtPosition(position).orElse(null));
    }

    private Optional<Ship> getShipAtPosition(Position position) {
        final Field fieldAtPosition = getFieldAtPosition(position);
        for (Ship ship : this.ships) {
            if (ship.getShipFields().contains(fieldAtPosition)) {
                return Optional.of(ship);
            }
        }
        return Optional.empty();
    }
}
