package org.filip.ai;

public class PlacementImpossibleException extends Exception {

    public PlacementImpossibleException() {
        super("There is no place for another ship on the board, please check the game configuration");
    }

}
