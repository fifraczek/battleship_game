package org.filip.ai;

import org.filip.board.Board;
import org.filip.board.InvalidPlacementException;
import org.filip.board.Position;
import org.filip.gui.InvalidPositionException;
import org.filip.ship.ShipRotation;
import org.filip.ship.ShipType;

import java.util.HashSet;
import java.util.Map;
import java.util.Random;
import java.util.Set;


public class AiComponent {

    private static final Random RANDOM = new Random();
    private static final int MAX_PLACE_ATTEMPTS = 10000;

    private AiComponent() {
    }

    public static void placeShipsRandomly(Board board, Map<ShipType, Integer> shipsCount) throws PlacementImpossibleException {
        for (var entry : shipsCount.entrySet()) {
            final int shipCount = entry.getValue();
            final ShipType shipType = entry.getKey();

            for (int i = 0; i < shipCount; i++) {
                placeShipOfSizeRandomly(board, shipType.getSize());
            }
        }
    }

    private static void placeShipOfSizeRandomly(Board board, int size) throws PlacementImpossibleException {
        boolean placedSuccessfully = false;
        int placeAttempts = 0;
        while (!placedSuccessfully) {
            final int startingColumn = RANDOM.nextInt(Board.BOARD_SIZE + 1 - size);
            final int startingRow = RANDOM.nextInt(Board.BOARD_SIZE + 1 - size);
            final ShipRotation rotation = RANDOM.nextBoolean() ? ShipRotation.VERTICAL : ShipRotation.HORIZONTAL;
            try {
                board.placeShip(prepareProposedPosition(size, startingColumn, startingRow, rotation));
                placedSuccessfully = true;
            } catch (InvalidPlacementException ignored) {
                placeAttempts++;
                if (placeAttempts > MAX_PLACE_ATTEMPTS) {
                    iterateThroughBoardAndPlaceShipIfPossible(board, size);
                    placedSuccessfully = true;
                }
            }
        }
    }

    private static Set<Position> prepareProposedPosition(int size, int startingColumn, int startingRow, ShipRotation rotation) {
        Set<Position> proposedPosition = new HashSet<>();
        for (int i = 0; i < size; i++) {
            if (ShipRotation.VERTICAL.equals(rotation)) {
                proposedPosition.add(new Position(startingColumn, startingRow + i));
            } else if (ShipRotation.HORIZONTAL.equals(rotation)) {
                proposedPosition.add(new Position(startingColumn + i, startingRow));
            }
        }
        return proposedPosition;
    }

    private static void iterateThroughBoardAndPlaceShipIfPossible(Board board, int size) throws PlacementImpossibleException {
        boolean shipPlaced = false;
        for (int startingColumn = 0; startingColumn < Board.BOARD_SIZE; startingColumn++) {
            for (int startingRow = 0; startingRow < Board.BOARD_SIZE; startingRow++) {
                if (shipPlaced) {
                    return;
                }
                try {
                    board.placeShip(prepareProposedPosition(size, startingColumn, startingRow, ShipRotation.VERTICAL));
                    shipPlaced = true;
                } catch (InvalidPlacementException | InvalidPositionException ignored) {
                    // if random placement will encounter exception - retry
                }
                try {
                    board.placeShip(prepareProposedPosition(size, startingColumn, startingRow, ShipRotation.HORIZONTAL));
                    shipPlaced = true;
                } catch (InvalidPlacementException | InvalidPositionException ignored) {
                    // if random placement will encounter exception - retry
                }
            }
        }
        throw new PlacementImpossibleException();
    }



}

