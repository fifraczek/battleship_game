package org.filip.ship;

import lombok.Getter;
import org.filip.board.InvalidPlacementException;
import org.filip.board.Position;

import java.util.Collection;
import java.util.function.Function;
import java.util.stream.Collectors;

@Getter
public enum ShipRotation {
    VERTICAL(Position::getRow),
    HORIZONTAL(Position::getColumn);

    private final Function<Position, Integer> indexExtractor;

    ShipRotation(Function<Position, Integer> indexExtractor) {
        this.indexExtractor = indexExtractor;
    }

    public static ShipRotation getRotationFromPosition(Collection<Position> positions) {
        boolean vertical = positions.stream()
                .map(Position::getColumn).collect(Collectors.toSet())
                .size() == 1;
        boolean horizontal = positions.stream()
                .map(Position::getRow)
                .collect(Collectors.toSet()).size() == 1;
        if (vertical) {
            return ShipRotation.VERTICAL;
        } else if (horizontal) {
            return ShipRotation.HORIZONTAL;
        } else {
            throw InvalidPlacementException.notInLine();
        }
    }

}
