package org.filip.ship;

import lombok.Getter;
import org.filip.board.Field;

import java.util.Set;

@Getter
public class Ship {

    private final Set<Field> shipFields;

    public Ship(Set<Field> shipFields) {
        this.shipFields = shipFields;
    }

    public ShipType getShipType() {
        return ShipType.getBySize(this.shipFields.size());
    }

    public boolean isActive() {
        return this.shipFields.stream()
                .anyMatch(o -> !o.isWasShot());
    }

    public boolean isSunk() {
        return this.shipFields.stream()
                .allMatch(Field::isWasShot);
    }


}
