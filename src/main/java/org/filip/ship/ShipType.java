package org.filip.ship;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.HashMap;
import java.util.Map;

@Getter
@RequiredArgsConstructor
public enum ShipType {

    BATTLESHIP(5, "Battleship", "B"),
    DESTROYER(4, "Destroyer", "D");

    private static final Map<Integer, ShipType> SHIP_TYPE_MAP = new HashMap<>();

    static {
        for (ShipType shipType : values()) {
            SHIP_TYPE_MAP.put(shipType.getSize(), shipType);
        }
    }

    private final int size;
    private final String label;
    private final String symbol;


    public static ShipType getBySize(int size) {
        return SHIP_TYPE_MAP.get(size);
    }

}
