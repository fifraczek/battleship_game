package org.filip.gui;

import lombok.SneakyThrows;
import org.filip.App;
import org.filip.GameStage;
import org.filip.InputReader;
import org.filip.board.Board;
import org.filip.ship.ShipType;

public class GUI {

    public void printBoards(Board board, String message) {
        this.clearConsole();
        print(this.getBoards(board, message));
    }

    public String getBoards(Board board, String message) {
        StringBuilder builder = new StringBuilder();
        builder.append(GuiElementsBuilder.getBoardTitle());
        builder.append(GuiElementsBuilder.getColumnHeaders());
        for (int row = 0; row < Board.BOARD_SIZE; row++) {
            builder.append(GuiElementsBuilder.getRows(row, board));
        }
        builder.append(GuiElementsBuilder.getColumnHeaders());
        builder.append(GuiElementsBuilder.getLine("-"));
        builder.append(GuiElementsBuilder.getShipsDeployedTitle());
        for (ShipType shipType : ShipType.values()) {
            builder.append(GuiElementsBuilder.getShipStats(shipType, board));
        }
        builder.append(GuiElementsBuilder.getLine("-"));
        builder.append(GuiElementsBuilder.getLine(" "));
        builder.append(FieldSymbol.getLegendText());
        builder.append(GuiElementsBuilder.getMessageBoard(message));

        return builder.toString();
    }

    public void printWinner(GameStage stage) {
        print(
                stage.getConsoleColors().getColoredOutput(String.format("=====> %s <=====", stage.getMessage()))
        );
        printPressAnyKey();
    }

    @SneakyThrows
    public void clearConsole() {
        if (!App.isWindowsMode()) {
            print("\033[H\033[2J");
        }
        System.out.flush();
    }

    public void print(String message) {
        System.out.println(message);
    }

    public void clearConsoleAndPrint(String message) {
        clearConsole();
        print(message);
    }

    public void printPressAnyKey() {
        print("");
        print("Press any key to continue");
        InputReader.readNextLine();
    }

}
