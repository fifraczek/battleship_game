package org.filip.gui;

import org.filip.board.Board;
import org.filip.board.FieldStatus;
import org.filip.board.Position;
import org.filip.board.ShootResult;
import org.filip.ship.ShipType;

import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class GuiElementsBuilder {

    private static final int BASIC_BOARD_WIDTH = 8 + 3 * Board.BOARD_SIZE;

    private GuiElementsBuilder() {
    }

    public static String getMessage(ShootResult result) {
        if (FieldStatus.HIT.equals(result.getFieldStatus())) {
            return ConsoleColors.GREEN.getColoredOutput(
                    "You have hit the " + result.getShipType().getLabel() + (result.isShipSunk() ? ", it has been sunk!" : "!"));
        } else {
            return ConsoleColors.CYAN.getColoredOutput("You have missed");
        }
    }

    public static String getPositionLabel(Position position) {
        return "" + (char) (position.getColumn() + Board.A_ASCII_INDEX) + (position.getRow() + 1);
    }


    public static String getMessageBoard(String message) {
        return message.isBlank() ? message : "\n    " + message + "\n";
    }

    public static String getShipStats(ShipType shipType, Board board) {
        final long activeShipsCount = board.activeShipByType(shipType);
        final long allShipsCount = board.shipByType(shipType);
        String playerShipTypeStats = String.format("    - %s %d/%d", shipType.getLabel(), activeShipsCount, allShipsCount);

        return fillToFitWidth(playerShipTypeStats, " ");
    }

    public static String getShipsDeployedTitle() {
        final String shipsDeployedString = "    SHIPS DEPLOYED:";
        return fillToFitWidth(shipsDeployedString, " ");

    }

    public static String getLine(String fillSymbol) {
        return fillToFitWidth("", fillSymbol);
    }

    public static String getRows(int rowNumber, Board board) {
        return prepareRowForBoard(board, rowNumber);
    }

    public static String prepareRowForBoard(Board board, int rowNumber) {
        final String rowSymbols = IntStream.range(0, Board.BOARD_SIZE)
                .mapToObj(col ->
                        board.getSymbolOfPosition(new Position(col, rowNumber)))
                .collect(Collectors.joining());
        return String.format("[%2d]", rowNumber + 1) + rowSymbols + String.format("[%2d]", rowNumber + 1) + "\n";
    }

    public static String getColumnHeaders() {
        final String columnHeaders = IntStream.range(0, Board.BOARD_SIZE)
                .mapToObj(o -> "[" + (char) (o + Board.A_ASCII_INDEX) + "]")
                .collect(Collectors.joining());
        return fillToFitWidth("[--]" + columnHeaders + "[--]", " ");
    }

    public static String getBoardTitle() {
        return fillToFitWidth("== Game board ", "=");
    }

    public static String fillToFitWidth(String initialString, String fillSymbol) {
        return initialString + fillSymbol.repeat(BASIC_BOARD_WIDTH - initialString.length()) + "\n";
    }

}
