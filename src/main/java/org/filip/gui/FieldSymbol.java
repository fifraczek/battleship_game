package org.filip.gui;

import lombok.Getter;
import org.filip.board.FieldStatus;
import org.filip.ship.Ship;

import java.util.Arrays;
import java.util.EnumMap;
import java.util.function.Function;
import java.util.stream.Collectors;

@Getter
public enum FieldSymbol {

    NOT_SHOT(FieldStatus.NOT_SHOT , "   ", "not shot"),
    HIT(FieldStatus.HIT, ConsoleColors.RED.getColoredOutput("(x)"), "shot, ship hit"),
    MISSED(FieldStatus.MISSED, ConsoleColors.CYAN.getColoredOutput("(~)"), "shot, missed");

    private static final EnumMap<FieldStatus, FieldSymbol> SYMBOLS_MAP = new EnumMap<>(
            Arrays.stream(values()).collect(Collectors.toMap(FieldSymbol::getStatus, Function.identity()))
    );

    private final FieldStatus status;
    private final String symbol;
    private final String label;

    static {
        for (FieldSymbol symbol : values()) {
            SYMBOLS_MAP.put(symbol.getStatus(), symbol);
        }
    }

    FieldSymbol(FieldStatus status, String symbol, String label) {
        this.status = status;
        this.symbol = symbol;
        this.label = label;
    }

    public static FieldSymbol findByStatus(FieldStatus status) {
        return SYMBOLS_MAP.get(status);
    }

    public String getTextSymbol(Ship ship) {
        return ship != null ?
                this.getSymbol().replace("x", ship.getShipType().getSymbol()) :
                this.getSymbol();
    }

    public static String getLegendText() {
        StringBuilder builder = new StringBuilder("  ");
        for (FieldSymbol symbol : values()) {
            builder.append(String.format("'%s' - %s; ", symbol.getSymbol(), symbol.getLabel()));
        }
        builder.append("\n");
        return builder.toString();
    }



}
