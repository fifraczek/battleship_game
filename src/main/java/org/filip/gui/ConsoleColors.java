package org.filip.gui;

import lombok.RequiredArgsConstructor;
import org.filip.App;

@RequiredArgsConstructor
public enum ConsoleColors {
    RED("\u001B[31m"),
    GREEN("\u001B[32m"),
    CYAN("\u001B[36m");

    public static final String ANSI_RESET = "\u001B[0m";

    public final String colorCode;

    public String getColoredOutput(String text) {
        return App.isWindowsMode() ? text : this.colorCode + text + ANSI_RESET;
    }

}
