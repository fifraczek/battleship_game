package org.filip.gui;

import org.filip.board.Board;

public class InvalidPositionException extends RuntimeException {

    private static final String INVALID_POSITION_INFO =
            String.format("The provided position is invalid. Column range: A - %s; Row range: 1 - %d", Board.LAST_COLUMN, Board.BOARD_SIZE);

    public InvalidPositionException() {
        super(INVALID_POSITION_INFO);
    }

}
