package org.filip;

import lombok.Getter;
import org.filip.ai.AiComponent;
import org.filip.ai.PlacementImpossibleException;
import org.filip.board.AlreadyShotException;
import org.filip.board.Board;
import org.filip.board.Position;
import org.filip.board.ShootResult;
import org.filip.gui.ConsoleColors;
import org.filip.gui.GUI;
import org.filip.gui.GuiElementsBuilder;
import org.filip.gui.InvalidPositionException;
import org.filip.ship.ShipType;

import java.util.EnumMap;
import java.util.Map;

public class Game {

    private static final EnumMap<ShipType, Integer> SHIPS_COUNT = new EnumMap<>(Map.ofEntries(
            Map.entry(ShipType.BATTLESHIP, 1),
            Map.entry(ShipType.DESTROYER, 2)
    ));

    @Getter
    private final Board board;
    private final GUI gui;
    private String message;


    public Game() {
        this.board = new Board();
        this.gui = new GUI();
        this.clearMessage();
    }

    public static Map<ShipType, Integer> getInitialShipsCount() {
        return SHIPS_COUNT;
    }

    public boolean run() throws PlacementImpossibleException {
        printGreetings();
        placeShipsOnBoard(this.board);
        while (this.isOngoing()) {
            makeTurn();
        }
        printWinner();
        return readStartAgain();
    }

    protected void printGreetings() {
        gui.clearConsoleAndPrint("Welcome to the BATTLESHIP game!");
        gui.printPressAnyKey();
    }

    protected void printWinner() {
        gui.clearConsole();
        gui.printBoards(this.board, this.message);
        gui.printWinner(GameStage.getStatus(this));
    }

    protected void makeTurn() {
        readAndMakePlayersShot();
    }

    private boolean isOngoing() {
        return GameStage.ONGOING.equals(GameStage.getStatus(this));
    }

    protected void readAndMakePlayersShot() {
        boolean shotFired = false;
        while (!shotFired) {
            try {
                gui.printBoards(this.board, this.message);
                this.clearMessage();

                gui.print("Please type one position you want to shoot (e.g. 'A1'):");
                final Position shootPosition = new Position(InputReader.readNextLine());
                final ShootResult shootResult = this.board.shootPosition(shootPosition);
                this.message = GuiElementsBuilder.getMessage(shootResult);
                shotFired = true;
            } catch (AlreadyShotException | InvalidPositionException e) {
                this.message = ConsoleColors.RED.getColoredOutput(e.getMessage());
            }
        }
    }

    protected void clearMessage() {
        this.message = "";
    }

    protected void placeShipsOnBoard(Board board) throws PlacementImpossibleException {
        AiComponent.placeShipsRandomly(board, SHIPS_COUNT);
    }

    protected boolean readStartAgain() {
        final String continueGameString = "y";
        gui.clearConsoleAndPrint(
                String.format("The game has ended. Please type '%s' if you want to play again, any other text - exit", continueGameString)
        );
        String input = InputReader.readNextLine();
        return continueGameString.equals(input);
    }
}
