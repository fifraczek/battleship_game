package org.filip;

import org.filip.board.Position;

import java.util.Arrays;
import java.util.Scanner;
import java.util.Set;
import java.util.stream.Collectors;

public class InputReader {

    private InputReader() {
    }

    private static final String POSITIONS_DELIMITER = ";";
    private static final Scanner SCANNER = new Scanner(System.in);

    public static Set<Position> toPositionsSet(String positionsString) {
        return Arrays.stream(positionsString.split(POSITIONS_DELIMITER))
                .map(Position::new)
                .collect(Collectors.toSet());

    }

    public static String readNextLine() {
        return SCANNER.nextLine();
    }
}
