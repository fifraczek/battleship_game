package org.filip;

import lombok.Getter;
import org.filip.ai.PlacementImpossibleException;

/**
 * Hello world!
 *
 */
public class App 
{
    public static final String WINDOWS_MODE_FLAG = "windows-mode";
    @Getter
    private static boolean windowsMode;

    public static void main( String[] args ) throws PlacementImpossibleException {
        readWinCompatible(args);
        boolean startNewGame = true;

        while(startNewGame) {
            Game game = new Game();
            startNewGame = game.run();
        }

    }

    private static void readWinCompatible(String[] args) {
        windowsMode = args.length >= 1 && WINDOWS_MODE_FLAG.equals(args[0]);
    }
}
