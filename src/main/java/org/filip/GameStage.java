package org.filip;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.filip.gui.ConsoleColors;

@Getter
@RequiredArgsConstructor
public enum GameStage {

    ONGOING("", ConsoleColors.CYAN),
    PLAYER_WINS("YOU WIN", ConsoleColors.GREEN);

    private final String message;
    private final ConsoleColors consoleColors;

    public static GameStage getStatus(Game game) {
        if (game.getBoard().allShipsSunk() ) {
            return PLAYER_WINS;
        } else {
            return ONGOING;
        }
    }
}
