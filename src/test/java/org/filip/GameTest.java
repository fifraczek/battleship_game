package org.filip;

import org.filip.board.Position;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class GameTest {

    private Game game;

    @BeforeEach
    void init() {
        initGameWithOneShipOnEachBoard();
    }

    void initGameWithOneShipOnEachBoard() {
        this.game = new Game();
        this.game.getBoard().placeShip(InputReader.toPositionsSet("A1;A2;A3;A4"));
    }

    @Test
    void shouldBeOngoingGame() {
        assertEquals(GameStage.ONGOING, GameStage.getStatus(game));
    }

    @Test
    void shouldBeWonGame() {
        this.game.getBoard().shootPosition(new Position("A1"));
        this.game.getBoard().shootPosition(new Position("A2"));
        this.game.getBoard().shootPosition(new Position("A3"));
        this.game.getBoard().shootPosition(new Position("A4"));
        assertEquals(GameStage.PLAYER_WINS, GameStage.getStatus(game));
    }

}
