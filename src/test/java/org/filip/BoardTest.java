package org.filip;


import org.filip.board.AlreadyShotException;
import org.filip.board.Board;
import org.filip.board.FieldStatus;
import org.filip.board.InvalidPlacementException;
import org.filip.board.Position;
import org.filip.board.ShootResult;
import org.filip.gui.GuiElementsBuilder;
import org.filip.ship.ShipType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.filip.InputReader.toPositionsSet;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

class BoardTest {

    private Board testBoard;

    @BeforeEach
    void init() {
        this.testBoard = new Board();
    }

    @Test
    void shouldPlaceShip() {
        assertEquals(0, testBoard.getShips().size());
        testBoard.placeShip(toPositionsSet("A1;A2"));
        testBoard.placeShip(toPositionsSet("B1;B2;B3;B4"));
        testBoard.placeShip(toPositionsSet("E5;F5;G5;H5"));
        testBoard.placeShip(toPositionsSet("E10;F10;G10;H10;I10"));
        assertEquals(4, testBoard.getShips().size());
    }

    @Test
    void shouldValidateDuplicates() {
        assertAll(
                () -> assertEquals(InvalidPlacementException.positionDuplicated("A1"),
                        assertThrows(InvalidPlacementException.class, () -> testBoard.placeShip(toPositionsSet("A1;A1")))),
                () -> assertEquals(InvalidPlacementException.positionDuplicated("A1"),
                        assertThrows(InvalidPlacementException.class, () -> testBoard.placeShip(toPositionsSet("A1;B1;A1")))),
                () -> assertEquals(InvalidPlacementException.positionDuplicated("A1, B1"),
                        assertThrows(InvalidPlacementException.class, () -> testBoard.placeShip(toPositionsSet("A1;B1;A1;B1"))))
        );
    }

    @Test
    void shouldValidateRotation() {
        assertAll(
                () -> assertEquals(InvalidPlacementException.notInLine(),
                        assertThrows(InvalidPlacementException.class, () -> testBoard.placeShip(toPositionsSet("A1;B2")))),
                () -> assertEquals(InvalidPlacementException.notInLine(),
                        assertThrows(InvalidPlacementException.class, () -> testBoard.placeShip(toPositionsSet("C3;A1;B2"))))
        );
    }

    @Test
    void shouldValidateContinuity() {
        assertAll(
                () -> assertEquals(InvalidPlacementException.notContinuous(),
                        assertThrows(InvalidPlacementException.class, () -> testBoard.placeShip(toPositionsSet("A1;C1;D1")))),
                () -> assertEquals(InvalidPlacementException.notInLine(),
                        assertThrows(InvalidPlacementException.class, () -> testBoard.placeShip(toPositionsSet("J10;J8;J7"))))
        );
    }

    @Test
    void shouldValidateOccupiedFields() {
        testBoard.placeShip(toPositionsSet("A1;A2;A3;A4;A5;A6;A7;A8;A9;A10"));
        assertAll(
                () -> assertEquals(InvalidPlacementException.fieldOccupied("A1, A2, A3"),
                        assertThrows(InvalidPlacementException.class, () -> testBoard.placeShip(toPositionsSet("A1;A2;A3")))),
                () -> assertEquals(InvalidPlacementException.fieldOccupied("A1"),
                        assertThrows(InvalidPlacementException.class, () -> testBoard.placeShip(toPositionsSet("A1;B1;C1"))))
        );
    }

    @Test
    void shouldThrowAlreadyShotException() {
        testBoard.placeShip(toPositionsSet("A1"));
        testBoard.shootPosition(new Position("A1"));
        assertThrows(AlreadyShotException.class, () -> testBoard.shootPosition(new Position("A1")));
    }

    @Test
    void shouldShotShip() {
        testBoard.placeShip(toPositionsSet("A1;A2;A3;A4"));
        final ShootResult shootResult = testBoard.shootPosition(new Position("A1"));
        assertEquals(ShipType.DESTROYER, shootResult.getShipType());
        assertEquals(FieldStatus.HIT, shootResult.getFieldStatus());
        assertEquals("\u001B[32mYou have hit the Destroyer!\u001B[0m", GuiElementsBuilder.getMessage(shootResult));
    }

    @Test
    void shouldShotAndSinkShip() {
        testBoard.placeShip(toPositionsSet("A1;A2;A3;A4"));
        testBoard.shootPosition(new Position("A1"));
        testBoard.shootPosition(new Position("A2"));
        testBoard.shootPosition(new Position("A3"));
        final ShootResult shootResult = testBoard.shootPosition(new Position("A4"));
        assertEquals(ShipType.DESTROYER, shootResult.getShipType());
        assertEquals(FieldStatus.HIT, shootResult.getFieldStatus());
        assertTrue(shootResult.isShipSunk());
        assertEquals("\u001B[32mYou have hit the Destroyer, it has been sunk!\u001B[0m", GuiElementsBuilder.getMessage(shootResult));
    }

    @Test
    void shouldShotAndMiss() {
        testBoard.placeShip(toPositionsSet("A1"));
        final ShootResult shootResult = testBoard.shootPosition(new Position("B1"));
        assertNull(shootResult.getShipType());
        assertEquals(FieldStatus.MISSED, shootResult.getFieldStatus());
        assertEquals("\u001B[36mYou have missed\u001B[0m", GuiElementsBuilder.getMessage(shootResult));
    }

}
