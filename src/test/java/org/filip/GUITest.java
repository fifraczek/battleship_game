package org.filip;

import org.filip.board.Board;
import org.filip.board.Position;
import org.filip.gui.GUI;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class GUITest {

    private final static String EXPECTED_BOARD = """
            == Game board ========================
            [--][A][B][C][D][E][F][G][H][I][J][--]
            [ 1][31m(B)[0m[36m(~)[0m[36m(~)[0m[36m(~)[0m[36m(~)[0m[36m(~)[0m[36m(~)[0m[36m(~)[0m[36m(~)[0m[36m(~)[0m[ 1]
            [ 2][31m(B)[0m[36m(~)[0m[36m(~)[0m[36m(~)[0m[36m(~)[0m[36m(~)[0m[36m(~)[0m[36m(~)[0m[36m(~)[0m[36m(~)[0m[ 2]
            [ 3][31m(B)[0m[36m(~)[0m[36m(~)[0m[31m(D)[0m[31m(D)[0m[31m(D)[0m[31m(D)[0m[36m(~)[0m[36m(~)[0m[36m(~)[0m[ 3]
            [ 4][31m(B)[0m[36m(~)[0m[36m(~)[0m[36m(~)[0m[36m(~)[0m[36m(~)[0m[36m(~)[0m[36m(~)[0m[36m(~)[0m[36m(~)[0m[ 4]
            [ 5][31m(B)[0m[36m(~)[0m[36m(~)[0m[36m(~)[0m[36m(~)[0m[36m(~)[0m[36m(~)[0m[36m(~)[0m[36m(~)[0m[36m(~)[0m[ 5]
            [ 6][36m(~)[0m[36m(~)[0m[36m(~)[0m[36m(~)[0m[36m(~)[0m[36m(~)[0m[36m(~)[0m[36m(~)[0m[36m(~)[0m[36m(~)[0m[ 6]
            [ 7][36m(~)[0m[36m(~)[0m[36m(~)[0m[36m(~)[0m[36m(~)[0m[36m(~)[0m[36m(~)[0m[36m(~)[0m[36m(~)[0m[36m(~)[0m[ 7]
            [ 8][36m(~)[0m[36m(~)[0m[36m(~)[0m[36m(~)[0m[36m(~)[0m[36m(~)[0m[36m(~)[0m[36m(~)[0m[36m(~)[0m[36m(~)[0m[ 8]
            [ 9][36m(~)[0m[36m(~)[0m[36m(~)[0m[36m(~)[0m[36m(~)[0m[36m(~)[0m[36m(~)[0m[36m(~)[0m[36m(~)[0m[36m(~)[0m[ 9]
            [10][36m(~)[0m[36m(~)[0m[36m(~)[0m[36m(~)[0m[36m(~)[0m[31m(D)[0m[31m(D)[0m[31m(D)[0m[31m(D)[0m[36m(~)[0m[10]
            [--][A][B][C][D][E][F][G][H][I][J][--]
            --------------------------------------
                SHIPS DEPLOYED:                  \s
                - Battleship 0/1                 \s
                - Destroyer 0/2                  \s
            --------------------------------------
                                                 \s
              '   ' - not shot; '[31m(x)[0m' - shot, ship hit; '[36m(~)[0m' - shot, missed;\s
                        
                Test message
            """;

    @Test
    void testGUI() {
        final Game game = new Game();
        final GUI gui = new GUI();
        game.getBoard().placeShip(InputReader.toPositionsSet("A1;A2;A3;A4;A5"));
        game.getBoard().placeShip(InputReader.toPositionsSet("D3;E3;F3;G3"));
        game.getBoard().placeShip(InputReader.toPositionsSet("F10;G10;H10;I10"));
        shotAll(game.getBoard());
        assertEquals(EXPECTED_BOARD, gui.getBoards(game.getBoard(), "Test message"));
    }

    private void shotAll(Board board) {
        for (int i = 0; i < Board.BOARD_SIZE; i++) {
            for (int j = 0; j < Board.BOARD_SIZE; j++) {
                board.shootPosition(new Position(i, j));
            }
        }
    }


}
