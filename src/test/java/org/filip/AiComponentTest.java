package org.filip;

import org.filip.ai.AiComponent;
import org.filip.ai.PlacementImpossibleException;
import org.filip.board.Board;
import org.filip.ship.ShipType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.EnumMap;
import java.util.Map;

import static org.filip.InputReader.toPositionsSet;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class AiComponentTest {

    private Board testBoard;

    @BeforeEach
    void init() {
        this.testBoard = new Board();
    }

    @Test
    void shouldPlaceShips() throws PlacementImpossibleException {
        AiComponent.placeShipsRandomly(testBoard, Game.getInitialShipsCount());
        int totalShipsCount = Game.getInitialShipsCount().values().stream().mapToInt(o -> o).sum();
        assertEquals(totalShipsCount, testBoard.getShips().size());
    }

    @Test
    void shouldPlaceShipWhileOnePossibleSpotLeft() throws PlacementImpossibleException {
        Map<ShipType, Integer> testMap = new EnumMap<>(ShipType.class);
        testMap.put(ShipType.DESTROYER, 1);

        fillAlmostWholeBoardWithShips();
        AiComponent.placeShipsRandomly(testBoard, testMap);
        assertEquals(11, testBoard.getShips().size());
    }

    @Test
    void shouldThrowExceptionWhenBoardIsFull() {
        fillWholeBoardWithShips();
        assertThrows(PlacementImpossibleException.class, () -> AiComponent.placeShipsRandomly(testBoard, Game.getInitialShipsCount()));
    }

    private void fillWholeBoardWithShips() {
        testBoard.placeShip(toPositionsSet("A1;A2;A3;A4;A5;A6;A7;A8;A9;A10"));
        testBoard.placeShip(toPositionsSet("B1;B2;B3;B4;B5;B6;B7;B8;B9;B10"));
        testBoard.placeShip(toPositionsSet("C1;C2;C3;C4;C5;C6;C7;C8;C9;C10"));
        testBoard.placeShip(toPositionsSet("D1;D2;D3;D4;D5;D6;D7;D8;D9;D10"));
        testBoard.placeShip(toPositionsSet("E1;E2;E3;E4;E5;E6;E7;E8;E9;E10"));
        testBoard.placeShip(toPositionsSet("F1;F2;F3;F4;F5;F6;F7;F8;F9;F10"));
        testBoard.placeShip(toPositionsSet("G1;G2;G3;G4;G5;G6;G7;G8;G9;G10"));
        testBoard.placeShip(toPositionsSet("H1;H2;H3;H4;H5;H6;H7;H8;H9;H10"));
        testBoard.placeShip(toPositionsSet("I1;I2;I3;I4;I5;I6;I7;I8;I9;I10"));
        testBoard.placeShip(toPositionsSet("J1;J2;J3;J4;J5;J6;J7;J8;J9;J10"));
    }

    private void fillAlmostWholeBoardWithShips() {
        testBoard.placeShip(toPositionsSet("A1;A2;A3;A4;A5;A6;A7;A8;A9;A10"));
        testBoard.placeShip(toPositionsSet("B1;B2;B3;B4;B5;B6;B7;B8;B9;B10"));
        testBoard.placeShip(toPositionsSet("C1;C2;C3;C4;C5;C6;C7;C8;C9;C10"));
        testBoard.placeShip(toPositionsSet("D1;D2;D3;D4;D5;D6;D7;D8;D9;D10"));
        testBoard.placeShip(toPositionsSet("E1;E2;E3;E4;E5;E6;E7;E8;E9;E10"));
        testBoard.placeShip(toPositionsSet("F1;F2;F3;F4;F5;F6;F7;F8;F9;F10"));
        testBoard.placeShip(toPositionsSet("G1;G2;G3;G4;G5;G6;G7;G8;G9;G10"));
        testBoard.placeShip(toPositionsSet("H1;H2;H3;H4;H5;H6;H7;H8;H9;H10"));
        testBoard.placeShip(toPositionsSet("I1;I2;I3;I4;I5;I6;I7;I8;I9;I10"));
        testBoard.placeShip(toPositionsSet("J1;J2;J3;J4;J5;J6"));
    }


}
