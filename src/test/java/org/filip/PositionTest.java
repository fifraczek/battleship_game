package org.filip;

import org.filip.board.Position;
import org.filip.gui.InvalidPositionException;
import org.filip.gui.GuiElementsBuilder;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class PositionTest {

    @Test
    void shouldReadPositionFromString() {
        Position p1 = new Position("A1");
        Position p2 = new Position("E8");
        Position p3 = new Position("J10");

        assertAll(
                () -> assertEquals(0, p1.getColumn()),
                () -> assertEquals(0, p1.getRow()),
                () -> assertEquals(4, p2.getColumn()),
                () -> assertEquals(7, p2.getRow()),
                () -> assertEquals(9, p3.getColumn()),
                () -> assertEquals(9, p3.getRow())
        );
    }

    @Test
    void shouldThrowExceptionOnInvalidPosition() {
        assertThrows(InvalidPositionException.class, () -> new Position(""));
        assertThrows(InvalidPositionException.class, () -> new Position("AD1"));
        assertThrows(InvalidPositionException.class, () -> new Position("B100"));
        assertThrows(InvalidPositionException.class, () -> new Position("&100"));
        assertThrows(InvalidPositionException.class, () -> new Position("C$"));
    }

    @Test
    void shouldReturnLabel() {
        final String position1String = "B2";
        final String position2String = "C9";
        Position p1 = new Position(position1String);
        Position p2 = new Position(position2String);

        assertAll(
                () -> assertEquals(position1String, GuiElementsBuilder.getPositionLabel(p1)),
                () -> assertEquals(position2String, GuiElementsBuilder.getPositionLabel(p2))
        );
    }

}
