## BATTLESHIP GAME 

### Build and run with docker *(recommended)*
>**Prerequisites:** docker CLI installed

Execute respectively `./build.cmd` and `./run.cmd` in the main project directory.

Tested on version: `Docker version 20.10.12, build e91ed57`

### Build and run with maven and java
>**Prerequisites:** JDK and maven installed

Execute :
- `mvn package` for build 
- `java -cp target\battleship-1.0-SNAPSHOT.jar org.filip.App` for run
  
in the main project directory.

It is recommended to run this application on unix-like system or use unix terminals on Windows (e.g. cygwin, git bash or some wsl shell).

**Console issues:** Windows PowerShell and Command Line doesn't support some console features.

In order to disable them please add `windows-mode` flag at the end of run command:
```
java -cp target\battleship-1.0-SNAPSHOT.jar org.filip.App windows-mode
```

Tested on java version:
```
openjdk 17.0.2 2022-01-18
OpenJDK Runtime Environment (build 17.0.2+8-86)
OpenJDK 64-Bit Server VM (build 17.0.2+8-86, mixed mode, sharing)
```

Tested on maven version:
```
Apache Maven 3.6.3 (cecedd343002696d0abb50b32b541b8a6ba2883f)
Maven home: C:\Program Files\apache-maven-3.6.3\bin\..
Java version: 17.0.2, vendor: Oracle Corporation, runtime: C:\Program Files\jdk-17.0.2
Default locale: pl_PL, platform encoding: Cp1250
OS name: "windows 11", version: "10.0", arch: "amd64", family: "windows"
```
