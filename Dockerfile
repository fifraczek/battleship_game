FROM maven:3.8.4-openjdk-17-slim@sha256:76b11de3a90a9dd4b2b1765850087296ec630c16636c91f0181d2fb7859f8502  AS build
RUN mkdir /project
COPY . /project
WORKDIR /project
RUN mvn clean package

FROM openjdk:17.0.2-slim@sha256:99724f585168668f673df4008bff9d67c1f8da32e67557f23ac2a968843aa129
RUN mkdir /app
COPY --from=build /project/target/battleship-1.0-SNAPSHOT.jar /app/battleship-1.0-SNAPSHOT.jar
WORKDIR /app
CMD ["java", "-cp", "battleship-1.0-SNAPSHOT.jar", "org.filip.App"]
